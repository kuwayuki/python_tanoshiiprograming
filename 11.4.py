import turtle
t = turtle.Pen()

def mysquare(size, filled):
  if filled == True:
    t.begin_fill()
  for x in range(1, 5):
    t.forward(size)
    t.left(90)
  if filled == True:
    t.end_fill()

mysquare(25, True)
mysquare(50, False)
mysquare(75, True)
mysquare(100, False)
mysquare(125, True)
mysquare(150, False)

input("push enter")