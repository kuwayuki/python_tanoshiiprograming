import turtle
t = turtle.Pen()
def mycircle(red, green, blue):
  t.color(red, green, blue)
  t.begin_fill()
  t.circle(50)
  t.end_fill()

mycircle(0, 1, 0)
mycircle(1, 0, 0)
mycircle(0, 0.5, 0)
mycircle(1, 0, 0)
mycircle(0.5, 0, 0)
mycircle(0, 0, 1)
mycircle(0, 0, 0.5)
mycircle(0.9, 0.75, 0)
mycircle(1, 0.7, 0.75)
mycircle(1, 0.5, 0)
mycircle(0.9, 0.5, 0.15)
mycircle(0, 0, 0)
mycircle(1, 1, 1)

input("press any key to exit")