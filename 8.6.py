class Things:
  pass

class Animate(Things):
  pass

class Animals(Animate):
  def breathe(self):
    print("breathing")
  def move(self):
    print("moving")
  def eat_food(self):
    print("eating food")

class Mammals(Animals):
  def feed_young_with_milk(self):
    print("feeding young")

class Giraffes(Mammals):
  def find_food(self):
    self.move()
    print("I've found food!")
    self.eat_food()
  def eat_leaves_from_trees(self):
    self.eat_food()
  def dance(self):
    print("left foot forward")
    print("left foot back")
    print("right foot forward")
    print("right foot back")
    print("left foot back")
    print("right foot back")
    print("right foot forward")
    print("left foot forward")

reginald = Giraffes()
reginald.dance()