class Things:#親クラスを定義
  pass

class Animate(Things):#親クラスを元に子クラスを定義
  pass

class Animals(Animate):#親クラスを元に子クラスを定義
  def breathe(self):#引数ありの関数を定義
    print("breathing")#戻り値を使ったときの呼び出すコードを表示
  def move(self):#引数ありの関数を定義
    print("moving")#戻り値を使ったときの呼び出すコードを表示
  def eat_food(self):#引数ありの関数を定義
    print("eating food")#戻り値を使ったときの呼び出すコードを表示

class Mammals(Animals):#親クラスを元に子クラスを定義
  def feed_young_with_milk(self):#引数ありの関数を定義
    print("feeding young")#戻り値を使ったときの呼び出すコードを表示

class Giraffes(Mammals):#親クラスを元に子クラスを定義
  def find_food(self):#引数ありの関数を定義
    self.move()#戻り値を使ったときの呼び出すコードを表示、ここで10も表示となる
    print("I've found food!")#戻り値を使ったときの呼び出すコードを表示
    self.eat_food()#戻り値を使ったときの呼び出すコードを表示、ここで12も表示となる
  def eat_leaves_from_trees(self):#引数ありの関数を定義
    self.eat_food()#戻り値を使ったときの呼び出すコードを表示、ここで12も表示となる
  def dance_a_jig(self):#引数ありの関数を定義
    self.move()#戻り値を使ったときの呼び出すコードを表示、ここで10も表示となる
    self.move()#戻り値を使ったときの呼び出すコードを表示、ここで10も表示となる
    self.move()#戻り値を使ったときの呼び出すコードを表示、ここで10も表示となる
    self.move()#戻り値を使ったときの呼び出すコードを表示、ここで10も表示となる

reginald = Giraffes()#オブジェクトを設定する
reginald.dance_a_jig()#戻り値？を記載して呼び出す
reginald.find_food()#戻り値？を記載して呼び出す
reginald.eat_leaves_from_trees()#戻り値？を記載して呼び出す
reginald.breathe()#戻り値？を記載して呼び出す