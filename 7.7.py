import sys#モジュールを使用するという命令
def silly_age_joke():#引数なしの関数を定義
  print("How old are you?")#相手に引数の変数を問いかけるための出力文を入力
  age = int(sys.stdin.readline())#年齢として変数を設定し、モジュールを組み込んだ関数を入力して数値に変換
  if age >= 10 and age <= 13:#条件式を設定し、ある年齢で出力する文章を設定
    print("What is 13 + 49 + 84 + 155 + 97? A headache!")
  else:#条件に添わない場合の出力する文章を設定
    print("Huh?")
silly_age_joke()#引数なしの関数を呼び出し、問いかけ文を出力・その後に入力した数値によって返答が変わる関数となっている