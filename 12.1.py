def hello():#メッセージを表示させる関数を定義
  print("hello there")#実際に表示させる内容を記載

from tkinter import *#表示させるモジュールを定義
tk = Tk()#クラスを定義
btn = Button(tk, text= "click me", command=hello)#クラスを用いてボタンを定義
btn.pack()#ボタンを表示させる関数

input("push key")