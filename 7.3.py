def variable_test():#引数なしの関数を定義
  first_variable = 10#関数の範囲内で、変数を設定して値を入力
  second_variable = 20#関数の範囲内で、変数を設定して値を入力
  return first_variable * second_variable#変数を使用した戻り値を設定、関数を呼び出したら戻り値が表示される
print(variable_test())#関数を使用した出力文を入力