import random#乱数のモジュール使用を宣言
num = random.randint(1, 100)#１〜１００までの範囲で変数をランダムに選択
while True:#正解が出るまで繰り返す無限の条件式を定義
  print("Guess a number between 1 and 100")#始まりのコメントを表示
  guess = input()#入力者が任意で入力するメソッドを定義
  i = int(guess)#入力された数値を整数に変換するメソッドを定義
  if i == num:#正解となる条件式を定義
    print("You guessed right")#正解の時のコメントを表示
    break#繰り返し終了
  elif i < num:#任意の数が選択した変数より小さい場合
    print("Try higher")#コメントを表示
  elif i > num:#任意の数が選択した変数より大きい場合
    print("Try lower")#コメントを表示
print("正解は", num, "です")