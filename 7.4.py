def spaceship_building(cans):#引数ありで関数を定義
  total_cans = 0#変数を設定して値を入力、初期値
  for week in range(1, 53):#１週間の関数を使用した繰り返し文を設定
    total_cans =total_cans + cans#繰り返しの計算式を設定
    print("Week %s = %s cans" % (week, total_cans))#変数や引数の変数を使用した出力文を設定
spaceship_building(2)#引数の変数を入力して関数を呼び出し
spaceship_building(13)#引数の変数を入力して関数を呼び出し