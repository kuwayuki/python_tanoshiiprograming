class Things:
  pass

class Animate(Things):
  pass

class Animals(Animate):
  def breathe(self):
    print("breathing")
  def move(self):
    print("moving")
  def eat_food(self):
    print("eating food")

class Mammals(Animals):
  def feed_young_with_milk(self):
    print("feeding young")

class Giraffes(Mammals):#哺乳類という親クラスからキリンという子クラスを定義
  def __init__(self, spots):#引数２つの関数を定義
    self.giraffe_spots = spots#呼び出したときの（キリンのブチ模様）値を引数のspotsと設定？

ozwald = Giraffes(100)#キリンの引数100をオズワルドと定義
gertrude = Giraffes(150)#キリンの引数150をガートルードと定義
print(ozwald.giraffe_spots)#オズワルドのブチ模様を表示
print(gertrude.giraffe_spots)#ガートルードのブチ模様を表示