from tkinter import *#図形のモジュールを定義
import random#ランダムモジュール
import time#時間モジュール

class Ball:#ボールのクラスを定義
  def __init__(self, canvas, color):#初期関数を定義、キャンバスと色を引数として設定
    self.canvas = canvas#変数canvas（左）に引数canvas（右）を設定、ボール自身をself
    self.id = canvas.create_oval(10, 10, 25, 25, fill = color)#ボールの座標と色を変数idとして定義
    self.canvas.move(self.id, 245, 100)#円を動かす関数を設定（対象のボールと動き、初期値）
    starts = [-3, -2, -1, 1, 2, 3]#ランダムに動かすようの数値列を設定
    random.shuffle(starts)#列の数値を元にランダムスタート
    self.x = starts[0]#ボールの速さX軸をランダム
    self.y = -3#ボールの速さY軸は固定
    self.canvas_height = self.canvas.winfo_height()#ボールとキャンバスの高さ設定を同一にした
    self.canvas_width = self.canvas.winfo_width()#ボールとキャンバスの幅設定を同一にした

  def draw(self):#描く関数を定義
    self.canvas.move(self.id, self.x, self.y)#ボールが動作するように見えるための連動引数を設定
    pos = self.canvas.coords(self.id)#識別番号が分かれば、キャンバス上の位置をXとYを組み合わせた座標として返す
    if pos[1] <= 0:#高さがキャンバス一番上にきた場合、
      self.y =6#Y軸のスピードを３に変更（跳ね返るように見える）
    if pos[3] >=self.canvas_height:#高さがキャンバスの一番下にきた場合、
      self.y = -3#Y軸のスピードをー３に変更（跳ね返るように見える）
    if pos[0] <=0:#幅がキャンバスの一番左にきた場合、
      self.x = 6#X軸のスピードを３に変更（跳ね返るように見える）
    if pos[2] >= self.canvas_width:#幅がキャンバスの一番右にきた場合、
      self.x = -3#X軸のスピードをー３に変更（跳ね返るように見える）

tk = Tk()#オブジェクトを定義
tk.title("Game")#タイトルを設定（ゲーム）
tk.resizable(0, 0)#キャンバスのサイズを設定（変更できないようにした）
tk.wm_attributes("-topmost", 1)#キャンバスを画面のどこに位置するかを設定（最前面）
canvas = Canvas(tk, width = 500, height = 400, bd = 0, highlightthickness = 0)#キャンバスのオブジェクトを定義（tkでサイズと外側線を設定）
canvas.pack()#キャンバスを表示
tk.update()#アニメーション表示のためにtkinterを初期化

ball = Ball(canvas, "red")#クラスからボールのオブジェクトを定義（キャンバス上で赤色）

while True:#正解の場合
  ball.draw()#ボールを描く
  tk.update_idletasks()#ウィンドウが閉じるまで繰り返す
  tk.update()#毎回初期化
  time.sleep(0.01)#１００分の１秒毎

# input("push return")