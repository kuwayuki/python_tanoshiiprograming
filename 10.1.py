class Animal:#クラスを定義（設計を定義）
  def __init__(self, species, number_of_legs, color):#初期値の引数が３つある関数を定義 
    self.species = species#紐付け
    self.number_of_legs = number_of_legs#紐付け
    self.color = color#紐付け

harry =Animal("hippogriff", 6, "pink")#それぞれの引数を入れたharryという名前をオブジェクト化（インスタンス化）

import copy#コピーするモジュールの使用を宣言
# harry =Animal("hippogriff", 6, "pink")#harryを表示
harriet = copy.copy(harry)#harryをコピーしたharrietという名前を定義
print(harry.species)#harryの種別を表示
print(harriet.species)#arrietの種別を表示

carrie = Animal("chimera", 4, "green polka dots")
billy =Animal("bogill", 0, "paisley")
my_animals = [harry, carrie, billy]
more_animals = copy.copy(my_animals)
print(more_animals[0].species)
print(more_animals[1].species)

my_animals[0].species = "ghoul"
print(my_animals[0].species)
print(more_animals[0].species)

sally =Animal("sphinx", 4, "sand")
my_animals.append(sally)#リストのに足して４つにした
print(len(my_animals))#lenというリストの長さを図る関数でリストの数を表示、浅いコピーだとリストまでは影響ない
print(len(more_animals))#lenというリストの長さを図る関数でリストの数を表示、浅いコピーだとリストまでは影響ない

more_animals = copy.deepcopy(my_animals)#深いコピーの関数でコピー
my_animals[0].species = "wyrm"
print(my_animals[0].species)
print(more_animals[0].species)