import turtle
t = turtle.Pen()

def draw_star(size, points):#任意の数値で図形をかく（大きさ、角の数）→多角形
  for x in range(1, points + 1):
    t.forward(size)
    t.left(360/points)

draw_star(120, 5)#任意の数値が３以上等の条件が追加で必要

t.reset()
def draw_mystar(size, points):#任意の数値で図形をかく（大きさ、角の数）→星
    for x in range(1, points * 2 + 1):
        t.forward(size)
        if x % 2 == 0:
            t.left(1575/points)#計算できず
        else:
            t.left(2025/points)#計算できず

draw_mystar(100, 9)#任意の数値が９意外の条件が追加で必要→任意で再現できず

input("push key")